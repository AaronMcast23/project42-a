﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ButtonOption : MonoBehaviour {


    public void trackSelector () {
        int x = Random.Range(1, 3);

        if (x == 1)
        {
            SceneManager.LoadScene(2);
        }
        if (x == 2)
        {
            SceneManager.LoadScene(3);
        }

    }

	public void PlayGame () {
		SceneManager.LoadScene (1);
	}

	public void MainMenu () {
		SceneManager.LoadScene (0);
	}

	//Below here are track selection buttons

	public void Track01 () {
		SceneManager.LoadScene (2);
	}

	public void Track02 () {
		SceneManager.LoadScene (3);
	}

    public void Quit()
    {
        ;
    }


}
